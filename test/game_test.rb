require_relative '../src/game'
require 'minitest/autorun'

class GameTest < Minitest::Test
  CORRECT_ANSWER = 'mySecretWord'
  INCORRECT_ANSWER = "incorrectAnswer"

  def setup
    @game = Game.new(CORRECT_ANSWER, 3);
  end

  def test_get_secret_returns_secret_word
    secret_word = CORRECT_ANSWER
    assert_equal(secret_word, @game.get_secret)
  end

  def test_at_start_is_over_returns_false
    assert_equal(false, @game.is_over)
  end

  def test_if_letter_exists_has_letter_returns_true
    assert_equal(true, @game.has_letter("m"))
  end

  def test_incorrect_tries_is_zero
    @game.has_letter("m")
    assert_equal(0, @game.get_incorrect_tries)
  end

  def test_incorrect_tries_is_one
    @game.has_letter("x")
    assert_equal(1, @game.get_incorrect_tries)
  end

  def test_incorrect_tries_is_two
    @game.has_letter("x")
    @game.has_letter("z")
    assert_equal(2, @game.get_incorrect_tries)
  end

  def test_if_letter_does_not_exist_has_letter_returns_false
    assert_equal(false, @game.has_letter("x"))
  end

  def test_is_answer_returns_false_for_incorrect_answer
    assert_equal(false, @game.is_answer(INCORRECT_ANSWER))
  end

  def test_is_answer_returns_true_for_correct_answer
    assert_equal(true, @game.is_answer(CORRECT_ANSWER))
  end

  def test_if_answer_is_correct_game_is_over_returns_true
    @game.is_answer(CORRECT_ANSWER)

    assert_equal(true, @game.is_over)
  end

  def test_if_answer_is_correct_is_winner_returns_true
    @game.is_answer(CORRECT_ANSWER)

    assert_equal(true, @game.is_winner)
  end

  def test_game_is_over_and_answer_not_found_is_winner_returns_false
    @game.has_letter("q")
    @game.has_letter("w")
    @game.has_letter("x")
    assert_equal(false, @game.is_winner)
  end

  def test_if_letter_exists_get_letter_positions_returns_them
    positions = @game.get_letter_positions("m")
    assert_equal(1, positions.length, "the size of the array should be one")
    assert_equal(0, positions[0], "the m is positioned on position 0")
  end

  def test_if_letter_does_not_exists_get_letter_positions_raises_error
    assert_raises(ArgumentError) {
      @game.get_letter_positions("x")
    }
  end

  def test_if_letter_exists_multiple_times_get_letter_positions_returns_all
    positions = @game.get_letter_positions("e")
    assert_equal(2, positions.length, "letter \"e\" should be found twice")
    assert_equal(3, positions[0], "letter \"e\" is found on position 3")
    assert_equal(6, positions[1], "letter \"e\" is found on position 3")
  end

  def test_if_player_tried_three_incorrect_times_game_is_over
    @game.has_letter("x")
    @game.has_letter("w")
    @game.has_letter("z")
    assert_equal(true, @game.is_over)
  end

  def test_game_is_over_returns_false_if_player_tried_one_correct_and_two_incorrect_times
    @game.has_letter("e")
    @game.has_letter("x")
    @game.has_letter("y")
    assert_equal(false, @game.is_over)
  end
end