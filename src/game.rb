class Game
  def initialize(secret, max_tries)
    @secret = secret
    @max_tries = max_tries
    @incorrect_tries = 0
    @answerFound = false
  end

  def get_secret
    @secret;
  end

  def get_incorrect_tries
    @incorrect_tries
  end

  def has_letter(letter)
    letter_found = @secret.index(letter) != nil
    if !letter_found
      @incorrect_tries += 1
    end
    letter_found
  end

  def get_letter_positions(letter)
    if @secret.index(letter) != nil
      find_letter_positions(letter)
    else
      raise_letter_not_found_exception(letter)
    end
  end

  def raise_letter_not_found_exception(letter)
    message = "Letter " + letter + " does not exist in word"
    raise ArgumentError, message
  end

  def find_letter_positions(letter)
    positions = []
    index = 0
    @secret.split(letter).each do |chunk|
      index += chunk.length + positions.length
      positions.push(index)
    end
    positions.delete_at(positions.length-1)

    positions
  end

  def is_over
    @answer_found || @incorrect_tries >= @max_tries
  end

  def is_answer(answer)
    @answer_found = answer == @secret
  end

  private :find_letter_positions, :raise_letter_not_found_exception

  def is_winner
    @answer_found == true
  end
end